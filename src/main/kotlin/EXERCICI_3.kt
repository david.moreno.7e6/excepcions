import java.io.IOException
import java.nio.file.Path

import java.util.Scanner
import kotlin.io.path.Path
import kotlin.io.path.readText

fun main(){
    val sc= Scanner(System.`in`)
    val nomFitxer= sc.next()
    checkFitxer(nomFitxer)
}

fun checkFitxer(nomFitxer: String){
    val fitxer= Path("./src/main/Fitxers/$nomFitxer")
    try {
        println(fitxer.readText())
    }
    catch (e: IOException){
        println("S'ha produït un error d'entrada/sortida:")
        println("${nomFitxer} (El sistema no puede encontrar el archivo especificado)")
    }
}
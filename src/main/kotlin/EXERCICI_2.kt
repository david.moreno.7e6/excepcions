import java.util.Scanner

fun main(){
    println("Introdueix el número:")
    val sc= Scanner(System.`in`)
    println("El resultat es: ${aDoubleoU(sc.next())}")
}

fun aDoubleoU(input: String): Double{
    var result= 0.0
    try {
        result= input.toDouble()
    }
    catch (e: NumberFormatException){
        result= 1.0
    }
    return  result
}
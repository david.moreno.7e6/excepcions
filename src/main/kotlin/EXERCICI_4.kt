import java.lang.IndexOutOfBoundsException
import java.util.*

/* Volem crear un programa que afegeixi el numero de comandes per hora que rebi una empresa per el torn de nit.
El programa consistirá en afegir a una list les comandes per hora per tal de gestionar l'excepció UnsupportedOperationException.
S'afegiran 8 comandes/hora ja que el torn de nit es des de les 00 a les 8 hores.
 */

fun main(){
    var comandes= mutableListOf<Int>()
    val sc= Scanner(System.`in`)
    var correct= false
    while (!correct){
        println("Selecciona l'opció")
        println("AFEGIR      ESBORRAR    SORTIR")
        val seleccio= sc.next()
        if(seleccio.uppercase()== "AFEGIR"){
            println("Escriu la comanda a afegir")
            comandes.add(afegir(comandes.toTypedArray(),sc, sc.nextInt()))
            if(comandes.size == 8){
                correct= true
            }
        }
        else if (seleccio.uppercase()== "ESBORRAR"){
            println("Selecciona l' hora a esborrar")
            println("""
                0-> 00:00 - 01:00
                1-> 01:00 - 02:00
                2-> 02:00 - 03:00
                3-> 03:00 - 04:00
                4-> 04:00 - 05:00
                5-> 05:00 - 06:00
                6-> 06:00 - 07:00
                7-> 07:00 - 08:00
            """.trimIndent())
            comandes = esborrar(comandes, sc.nextInt())
        }
        else if (seleccio.uppercase() == "SORTIR"){
            correct= true
        }
        else{
            println("No valid")
        }
    }

}


fun esborrar(comandes: MutableList<Int>, posicio: Int): MutableList<Int>{
    try {
        comandes.removeAt(posicio)
    }
    catch (e: IndexOutOfBoundsException){
        println("Posició no válida")
    }
    return comandes
}

fun afegir(comandes: Array<Int>, sc: Scanner, comanda: Int): Int{
    try {
        comandes.toMutableList().add(comanda)
    }
    catch (e: UnsupportedOperationException){
        println("No valid")
    }
    return comanda
}